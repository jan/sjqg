<?php
/**
 * @file defines a theme class for ScrollingJQueryGallery
 *
 * @author Jan Dittberner <jan@dittberner.info>
 *
 * @version $Id$
 *
 * Copyright (c) 2009 Jan Dittberner
 * Jan Dittberner IT-Consulting & -Solutions
 * Cottbuser Str. 1, D-01129 Dresden
 *
 * This file is part of the ScrollingJQueryGallery component of the
 * gnuviech-server.de Websitetools
 *
 * ScrollingJQueryGallery is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * ScrollingJQueryGallery is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ScrollingJQueryGallery.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

$dir = dirname(realpath(__file__));
require("$dir" . DIRECTORY_SEPARATOR . "template.class.php");

/**
 * Theme class.
 */
class Theme {
  /**
   * Theme type 'horizontal' or 'vertical'
   * @var string
   */
  var $themetype;

  /**
   * Size of preview images. Used for calculating preview image sizes.
   * @var int
   */
  var $previewsize;

  /**
   * Size of thumbnail images. Depending on the theme type this is
   * used for calculating the width (vertical themes) or height
   * (horizontal themes) of the thumbnal images.
   * @var int
   */
  var $thumbsize;

  var $name;
  var $template;
  var $config;

  /**
   * Constructor for themes. Expects a theme name and initializes the
   * internal state of the instance from the 'theme.ini' file in the
   * named theme's directory.
   * @param string $name the directory name of the theme
   */
  function __construct($name, &$configuration) {
    $themeini = realpath(implode(DIRECTORY_SEPARATOR,
      array($configuration['themedir'], $name, 'theme.ini')));
    if (!$themeini) {
      die("invalid theme $name");
    }
    $themeconfig = parse_ini_file($themeini);
    $this->name = $name;
    $this->themetype = $themeconfig['themetype'];
    $this->previewsize = intval($themeconfig['previewsize']);
    $this->thumbsize = intval($themeconfig['thumbsize']);
    $this->config = $configuration;
  }

  function getTemplate() {
    if (!$this->template) {
      $this->template = new Template();
      $this->template->assign('themepath',
        implode(DIRECTORY_SEPARATOR, array($this->config['themepath'], $this->name)));
    }
    return $this->template;
  }

  function display() {
    $this->template->display(implode(DIRECTORY_SEPARATOR,
      array($this->config['themedir'], $this->name, 'theme.php')));
  }
}
?>