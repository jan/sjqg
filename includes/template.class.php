<?php
/**
 * @file simple template system for ScrollingJQueryGallery
 *
 * @author Jan Dittberner <jan@dittberner.info>
 *
 * @version $Id$
 *
 * Copyright (c) 2009 Jan Dittberner
 * Jan Dittberner IT-Consulting & -Solutions
 * Cottbuser Str. 1, D-01129 Dresden
 *
 * This file is part of the ScrollingJQueryGallery component of the
 * gnuviech-server.de Websitetools
 *
 * ScrollingJQueryGallery is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * ScrollingJQueryGallery is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ScrollingJQueryGallery.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * This class provides a simple template mechanism. It was inspired by
 * http://mylittlehomepage.net/ueber-den-sinn-von-php-template-engines
 * (german).
 */
class Template {
  /**
   * associative array containing the template content.
   * @var array
   */
  var $content;

  /**
   * Assign a value to the template.
   * @param string $name variable name
   * @param mixed $value variable value
   */
  function assign($name, $value) {
    $this->content[$name] = $value;
  }

  /**
   * Display the given template file.
   * @param string $template template file name
   */
  function display($template) {
    if($this->content) {
      $content = $this->content;
    }
    include($template);
  }
}
?>