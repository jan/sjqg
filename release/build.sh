#!/bin/sh
#
# Build script for ScrollingJQueryGallery
#
# Author: Jan Dittberner <jan@dittberner.info>
#
# Copyright © 2009 Jan Dittberner
# Jan Dittberner IT-Consulting & -Solutions
# Cottbuser Str. 1, D-01129 Dresden, Germany
#
# This file is part of the ScrollingJQueryGallery component of the
# gnuviech-server.de Websitetools
#
# ScrollingJQueryGallery is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# ScrollingJQueryGallery is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScrollingJQueryGallery.  If not, see
# <http://www.gnu.org/licenses/>.

set -e

BASEPATH=`dirname "$0"`/..
VERSION="0.2"
PROJECT="scrollingjquerygallery"

BASEPATH=`realpath "${BASEPATH}"`
TEMPDIR=`mktemp -d`

ln -s "$BASEPATH" "${TEMPDIR}/${PROJECT}-${VERSION}"

# release tar file
TARFILE=`realpath "${BASEPATH}/../"`/${PROJECT}-${VERSION}.tar.gz
tar czhf "$TARFILE" -C "${TEMPDIR}" \
  --exclude '*/.git' --exclude '*/bilder' --exclude '.svn' \
  --exclude '*~' --exclude 'gallery.ini' \
  "${PROJECT}-${VERSION}"
gpg --detach-sign --armor "$TARFILE"
md5sum "$TARFILE" > "$TARFILE.md5"
echo "release is in $TARFILE"

# sample images tar file
TARFILE=`realpath "${BASEPATH}/../"`/${PROJECT}-resources-${VERSION}.tar.gz
tar czhf "$TARFILE" -C "${TEMPDIR}" \
  --exclude '.svn' "${PROJECT}-${VERSION}/bilder"
gpg --detach-sign --armor "$TARFILE"
md5sum "$TARFILE" > "$TARFILE.md5"
echo "resources are in $TARFILE"

rm -r "$TEMPDIR"
