/*
* Image scrolling JQuery code.
*
* Copyright (c) 2007, 2008, 2009 Jan Dittberner <jan@dittberner.info>
* Jan Dittberner IT-Consulting & -Solutions
* Cottbuser Str. 1, D-01129 Dresden
*
* This file is part of the ScrollingJQueryGallery component of the
* gnuviech-server.de Websitetools
*
* ScrollingJQueryGallery is free software: you can redistribute it
* and/or modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* ScrollingJQueryGallery is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ScrollingJQueryGallery.  If not, see
* <http://www.gnu.org/licenses/>.
*
* Version: $Id$
*/
var imgprefix = "bilder";

function getPathParts(imagesrc) {
    var filename = imagesrc.substring(imagesrc.lastIndexOf("/") + 1);
    var pathstart = imagesrc.substring(0, imagesrc.lastIndexOf("/"));
    // run through directories until imgprefix is found
    var dirstack = new Array();
    var current = pathstart.substring(pathstart.lastIndexOf("/") + 1);
    while (current != imgprefix) {
        dirstack.push(current);
        pathstart = pathstart.substring(0, pathstart.lastIndexOf("/"));
        current = pathstart.substring(pathstart.lastIndexOf("/") + 1);
    }
    var galleryname = dirstack.pop();
    var dirname = null;
    if (dirstack.length > 0) {
        dirname = dirstack.pop();
    }
    return {
        'filename'  : filename,
        'dirname'   : dirname,
        'gallery'   : galleryname,
        'pathstart' : pathstart
    };
}

function updateContentImage(pathParts) {
    $.get(basepath + "ajaxrequest.php", {
        "imagename"   : pathParts.filename,
        "galleryname" : pathParts.gallery
    }, function(data, textStatus) {
        $("#imagedescription").text(data["label"]);
        $('#content_main img').attr('alt', data.label).attr('src', data.preview[0]).attr('width', data.preview[1]).attr('height', data.preview[2]);
        $("#content_main a").attr("href", data["full"]).attr("title", data["label"]);
        document.title = data["title"];
    }, 'json');
}

$(document).ready(function() {
    if (themetype == 'horizontal') {
        $("#arrleft").mouseover(function() {
            $("#scrollable").animate({
                left: "0px"
            }, 500);
        }).mouseout(function() {
            $("#scrollable").stop();
        });
        $("#arrright").mouseover(function() {
            var offset = parseInt($("#imgscroller").css("width")) -
                parseInt($("#scrollable").css("width"));
            $("#scrollable").animate({
                left: offset + "px"
            }, 500);
        }).mouseout(function() {
            $("#scrollable").stop();
        });
    } else {
        $('#arrup').mouseover(function() {
            $('#scrollable').animate({
                top: "0px"
            }, 500);
        }).mouseout(function() {
            $('#scrollable').stop();
        });
        $('#arrdown').mouseover(function() {
            var offset = parseInt($('#imgscroller').css('height')) -
                parseInt($('#scrollable').css('height'));
            $('#scrollable').animate({
                top: offset + "px"
            }, 500);
        }).mouseout(function() {
            $('#scrollable').stop();
        });
    }
    $("#backbtn").click(function() {
        $("div.thumbnail img").each(function(i) {
            var curparts = getPathParts($("img#contentimg").attr("src"));
            var myparts = getPathParts($(this).attr("src"));
            if ((curparts.gallery == myparts.gallery) &&
                (curparts.filename == myparts.filename)) {
                var matched = $("div.thumbnail img");
                var prevparts;
                if (i > 0) {
                    prevparts = getPathParts(
                        $(matched.get(i-1)).attr("src"));
                } else {
                    prevparts = getPathParts(
                        $(matched.get(matched.length-1)).attr("src"));
                }
                updateContentImage(prevparts);
                return false;
            }
        });
    });
    $("#fwdbtn").click(function() {
        $("div.thumbnail img").each(function(i) {
            var curparts = getPathParts($("img#contentimg").attr("src"));
            var myparts = getPathParts($(this).attr("src"));
            if ((curparts.gallery == myparts.gallery) &&
                (curparts.filename == myparts.filename)) {
                var matched = $("div.thumbnail img");
                var nextparts;
                if (i < matched.length-1) {
                    nextparts = getPathParts(
                        $(matched.get(i+1)).attr("src"));
                } else {
                    nextparts = getPathParts(
                        $(matched.get(0)).attr("src"));
                }
                updateContentImage(nextparts);
                return false;
            }
        });
    });
    $("div.thumbnail img").mouseover(function() {
        updateContentImage(getPathParts(this.src));
    });
});
