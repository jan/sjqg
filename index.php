<?php
/**
 * @file Startseite für ScrollingJQueryGallery. Diese Datei erledigt
 * die Ausgabe im Browser.
 *
 * @author Jan Dittberner <jan@dittberner.info>, Jeremias Arnstadt
 * <douth024@googlemail.com>
 *
 * @version $Id$
 *
 * Copyright (c) 2008, 2009 Jan Dittberner
 * Jan Dittberner IT-Consulting & -Solutions
 * Cottbuser Str. 1, D-01129 Dresden
 *
 * Copyright (c) 2008 Jeremias Arnstadt
 *
 * This file is part of the ScrollingJQueryGallery component of the
 * gnuviech-server.de Websitetools
 *
 * ScrollingJQueryGallery is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * ScrollingJQueryGallery is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ScrollingJQueryGallery.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

$basedir = realpath(dirname(__file__));

/**
 * Inkludiert die Funktionsbibliothek.
 */
require($basedir . '/includes/galleryfunctions.php');

/**
 * Name der aktuellen Galerie.
 */
$gallery   = getCurrentGallery();

/**
 * Informationen zu den Thumbnail-Bildern der aktuellen Galerie.
 */
$thumbinfo = getThumbNailInfo($gallery);

$scripts = array($configuration['basepath'] . 'js/jquery.js',
                 $configuration['basepath'] . 'js/jquery.colorBlend.js',
                 $configuration['basepath'] . 'js/jquery.lightbox.js',
                 $configuration['basepath'] . 'scripts/ourhandlers.js');
$styles  = array();

$template = $theme->getTemplate();

if ($theme->themetype == 'horizontal') {
  $inlinestyles = sprintf("#scrollable { width:%dpx; }\n", $thumbinfo[0]);
  $inlinescript = array("var themetype='horizontal';");
} else {
  $inlinestyles = sprintf("#scrollable { height:%dpx; }\n", $thumbinfo[0]);
  $inlinescript = array("var themetype='vertical';");
}
$inlinescript[] = "var basepath='" . $configuration['basepath'] . "';";

$template->assign('scripts', $scripts);
$template->assign('styles', $styles);
$template->assign('inlinestyle', $inlinestyles);
$template->assign('inlinescript', implode("\n", $inlinescript));
$template->assign('title', getGalleryTitle($thumbinfo));
$template->assign('gallerylinks', getGalleryLinks());
$template->assign('thumbnails', getAllThumbnails($thumbinfo));
$template->assign('firstpreview', getFirstPreview($thumbinfo));
$template->assign('firstdescription', getFirstDescription($thumbinfo));
$template->assign('lang', 'de');
$template->assign('gallery', $gallery);

$theme->display();

?>