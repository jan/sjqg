OBJECTS = *.php includes/*.php themes/*/*.php dummy/*.php
LANGUAGES = de
TARGETFILES = $(foreach lang,$(LANGUAGES),locale/$(lang)/LC_MESSAGES/sjqg.mo)
POFILES = 

all:	$(TARGETFILES) $(POFILES)

%.mo: %.po
	msgfmt $< -o $@

%.po: locale/sjqg.pot
	if [ ! -d `dirname "$@"` ]; then mkdir -p `dirname "$@"`; fi
	if [ -f $@ ]; then \
		msgmerge -U $@ $<; \
	else \
		cp $< $@; \
	fi

locale/sjqg.pot: $(OBJECTS)
	xgettext -d sjqg -o sjqg.pot -p locale \
	 --from-code=UTF8 --sort-output \
	 --copyright-holder='Jan Dittberner <jan@dittberner.info>' \
	 --package-name='SJQG' --package-version='0.3' \
	 --language=PHP $(OBJECTS)

.SECONDARY:
