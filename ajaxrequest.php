<?php
  /**
   * @file
   * Behandlung von AJAX-Requests.
   *
   * @author Jan Dittberner <jan@dittberner.info>
   * @version \$Id$
   *
   * Copyright (c) 2007, 2008 Jan Dittberner <jan@dittberner.info>
   * Jan Dittberner IT-Consulting & -Solutions,
   * Cottbuser Str. 1, D-01129 Dresden
   *
   * This file is part of the ScrollingJQueryGallery component of the
   * gnuviech-server.de Websitetools
   *
   * ScrollingJQueryGallery is free software: you can redistribute it
   * and/or modify it under the terms of the GNU General Public
   * License as published by the Free Software Foundation, either
   * version 3 of the License, or (at your option) any later version.
   *
   * ScrollingJQueryGallery is distributed in the hope that it will be
   * useful, but WITHOUT ANY WARRANTY; without even the implied
   * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   * See the GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with ScrollingJQueryGallery.  If not, see
   * <http://www.gnu.org/licenses/>.
   */

  /**
   * Inkludiert die Funktionsbibliothek.
   */
require 'includes/galleryfunctions.php';

if (isset($_GET["imagename"]) && isset($_GET["galleryname"]) &&
    preg_match('/^[\w\d _-]+\.jp(e|)g$/', $_GET["imagename"]) &&
    preg_match(GALLERY_RE, $_GET["galleryname"])) {
  header("Content-Type: text/plain; charset=UTF-8");
  if ($imageInfo = getImageInfo($_GET["galleryname"], $_GET["imagename"])) {
    print json_encode($imageInfo);
  } else {
    header("HTTP/1.0 404 Not Found");
    header("Status: 404 Not Found");
  }
} else {
  header("HTTP/1.0 400 Bad Request");
  header("Status: 400 Bad Request");
}
