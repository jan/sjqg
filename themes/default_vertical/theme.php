<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $content['lang'] ; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $content['title']; ?></title>
<style type="text/css">
<?php print($content['inlinestyle']); ?>
</style>
<?php foreach ($content['styles'] as $style) { ?>
<link rel="stylesheet" type="text/css" href="<?php echo $style; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo $content['themepath']; ?>/css/main.css" />
<script type="text/javascript"><!--
<?php echo $content['inlinescript']; ?>

//--></script>
<?php foreach ($content['scripts'] as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<body>
  <ul id="menu"><?php foreach ($content['gallerylinks'] as $data) {
   printf('<li class="menu%s"><a class="menu" href="%s">%s </a></li>',
          ($data['gallery'] == $content['gallery']) ? ' active' : '',
          $data['url'], $data['label']);
  } ?></ul>
  <div id="slider">
    <div id="arrup">up</div>
    <div id="imgscroller">
      <div id="scrollable"><?php
   foreach($content['thumbnails'] as $thumb) {
   ?><div class="thumbnail"><img src="<?php echo $thumb['src']; ?>" alt="<?php echo $thumb['alt']; ?>" <?php echo $thumb['sizes']; ?> /></div><?php
 } ?></div></div>
    <div id="arrdown">down</div>
  </div>
  <div id="content_main"><?php
    $data = $content['firstpreview'];
    printf('<a class="lightbox" title="%s" href="%s" rel="lightbox">' .
           '<img id="contentimg" src="%s" alt="%s" %s /></a>',
           $data['title'], $data['full'], $data['src'], $data['alt'],
           $data['sizes']);
  ?></div>
  <div class="imgdescription" id="imagedescription"><?php
    echo $content['firstdescription']; 
  ?></div>
  <div id="content_nav">
    <span id="backbtn">Back</span>
    <span id="fwdbtn">Forward</span>
  </div>
</body>
</html>