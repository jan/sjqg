<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $content['lang'] ; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $content['title']; ?></title>
<style type="text/css">
<?php print($content['inlinestyle']); ?>

a {
	font-size: 9px;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
</style>
<?php foreach ($content['styles'] as $style) { ?>
<link rel="stylesheet" type="text/css" href="<?php echo $style; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo $content['themepath']; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $content['themepath']; ?>/css/format.css" />
<script type="text/javascript"><!--
<?php echo $content['inlinescript']; ?>

//--></script>
<?php foreach ($content['scripts'] as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<body>
  <div id="container">
    <div id="content">
      <ul id="menu"><?php
 foreach ($content['gallerylinks'] as $data) {
   printf('<li class="menu%s"><a class="menu" href="%s">%s </a></li>',
          ($data['gallery'] == $content['gallery']) ? ' active' : '',
          $data['url'], $data['label']);
 } ?></ul>
      <div id="content_container">
      <div id="slider">
        <div id="arrleft"><img src="<?php echo $content['themepath']; ?>/images/aro-lft.png" alt="nach links" width="10" height="65" /></div>
        <div id="imgscroller"><div id="scrollable"><?php
          // show Thumnails an
         foreach ($content['thumbnails'] as $thumbdata) {
 ?><div class="thumbnail"><img src="<?php echo $thumbdata['src']; ?>" alt="<?php echo $thumbdata['alt']; ?>" <?php echo $thumbdata['sizes']; ?> /></div><?php
 } ?></div></div>
        <div id="arrright"><img src="<?php echo $content['themepath']; ?>/images/aro-rt.png" alt="nach rechts" width="10" height="65" /></div>
      </div>
    </div>
    <div id="content_sub">
      <div id="content_main">
        <div class="bildposition"><?php
          // bindet die Bilder in den Hauptbereich ein
   $data = $content['firstpreview'];
          printf('<a  class="lightbox" title="%s" href="%s" rel="lightbox">' .
                 '<img id="contentimg" src="%s" alt="%s" %s /></a>',
                 $data['title'], $data['full'], $data['src'], $data['alt'],
                 $data['sizes']);
        ?></div>
      </div>
      <div id="content_nav">
        <img id="backbtn" src="<?php echo $content['themepath']; ?>/images/back.png" alt="back" class="back" width="10" height="10" /><img id="fwdbtn" src="<?php echo $content['themepath']; ?>/images/next.png" alt="next" class="next" width="10" height="10" />
      </div>
      <div class="imgdescription" id="imagedescription"><?php
        echo $content['firstdescription']; 
      ?></div>
    </div>
  </div> 
</div>
</body>
</html>